
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  name    = "matrix"
  commonx = yamldecode(sops_decrypt_file("${local.project.locals.data_dir}/common.sops.yml"))
  android = jsondecode(sops_decrypt_file("${local.project.locals.data_dir}/android.sops.json"))
  ios     = jsondecode(sops_decrypt_file("${local.project.locals.data_dir}/ios.sops.json"))

}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//matrix-ssm?ref=master"

  after_hook "after_hook" {
    commands = ["apply", "output"]
    execute  = ["sh", "-c", "${get_env("TERRAGRUNT_TFPATH")} output -json  kms_key_arn | jq -r '.' > ${local.project.locals.data_dir}/matrix-kms-key-arn"]
  }
}

dependency "rds" {
  config_path = "../rds"
}

dependency "smtp" {
  config_path = "../smtp"
}

dependency "s3_media" {
  config_path = "../s3-media"
}

dependency "certs" {
  config_path = "../letsencrypt"
}

dependency "route53" {
  config_path = "../route53-internal"
}


include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  smtp_server          = dependency.smtp.outputs.smtp_server
  smtp_username        = dependency.smtp.outputs.smtp_username
  smtp_password        = dependency.smtp.outputs.smtp_password
  smtp_port            = dependency.smtp.outputs.smtp_port
  smtp_from_address    = local.commonx.email_from_address
  smtp_from_name       = local.commonx.email_from_name
  certificate_pem      = dependency.certs.outputs.matrix_certificate_pem
  domain_name          = local.commonx.matrix_common_name
  internal_domain_name = dependency.route53.outputs.domain_name


  # ma1sd
  ma1sd_hostname    = local.commonx.matrix_common_name
  ma1sd_tos_url     = local.commonx.tos_url
  ma1sd_tos_name    = local.commonx.tos_name
  ma1sd_tos_version = local.commonx.tos_version

  # sygnal
  sygnal_ios         = local.ios.keys
  sygnal_android     = local.android.keys
  sygnal_db_name     = dependency.rds.outputs.sygnal_db_name
  sygnal_db_username = dependency.rds.outputs.sygnal_db_username
  sygnal_db_password = dependency.rds.outputs.sygnal_db_password
  sygnal_db_port     = dependency.rds.outputs.db_port
  sygnal_db_address  = dependency.rds.outputs.db_address

  # synapse
  synapse_db_name                = dependency.rds.outputs.synapse_db_name
  synapse_db_username            = dependency.rds.outputs.synapse_db_username
  synapse_db_password            = dependency.rds.outputs.synapse_db_password
  synapse_db_port                = dependency.rds.outputs.db_port
  synapse_db_address             = dependency.rds.outputs.db_address
  web_client_base_url            = "https://${local.commonx.riot_common_name}"
  macaroon_secret_key            = local.commonx.synapse.macaroon_secret_key
  password_config_pepper         = local.commonx.synapse.password_config_pepper
  shared_registration_secret     = local.commonx.synapse.shared_registration_secret
  synapse_signing_key            = local.commonx.synapse.synapse_signing_key
  synapse_disk_allocation_gb     = local.commonx.synapse.synapse_disk_allocation_gb
  manhole_enabled                = false
  federation_enabled             = true
  synapse_metrics_enabled        = true
  synapse_metrics_bind_addresses = ["0.0.0.0"]
  admin_contact_email            = local.commonx.synapse.admin_contact_email
  media_bucket_id                = dependency.s3_media.outputs.bucket_id
  media_bucket_arn               = dependency.s3_media.outputs.bucket_arn
}
